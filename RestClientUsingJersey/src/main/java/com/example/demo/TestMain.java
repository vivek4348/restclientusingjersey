package com.example.demo;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.example.Employee;
import com.example.JsonUtil;
import com.exostar.icert.cam.common.CompanyType;
import com.exostar.icert.cam.common.ResponseType;
import com.exostar.icert.cam.common.ServiceProviderType;
import com.exostar.icert.cam.common.UserType;
import com.exostar.icert.cam.company.companycreateupdaterequesttype.CompanyCreateUpdateRequestType;
import com.exostar.icert.cam.company.companydeleterequesttype.CompanyDeleteRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsrequesttype.GetUserAuthorizationsRequestType;
import com.exostar.icert.cam.user.getuserauthorizationsresponsetype.GetUserAuthorizationsResponseType;
import com.exostar.icert.cam.user.usercreateupdaterequesttype.UserCreateUpdateRequestType;
import com.exostar.icert.cam.user.userdeleterequesttype.UserDeleteRequestType;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.apache.ApacheHttpClient;

public class TestMain {

	private static ApacheHttpClient cli = ApacheHttpClient.create();

	protected ApacheHttpClient getClient() {
		return cli;
	}
	
	public static void main(String[] args) throws JAXBException {
		/*String userId = "threen_6572@fis.evincible.com";
		String spId = "TSP";
		String authorizationsJson = getAllAuthorizationsForUserAndSP(userId, spId);
		System.out.println(authorizationsJson);*/
		
		/*Boolean createCompany = createCompany();
		System.out.println("Company Created: " + createCompany);*/
		
		 /*Boolean updateCompany = updateCompany("EXO117231547", "HOME");
		System.out.println("Company Updated: " + updateCompany);*/
		
		Boolean deleteCompany = deleteCompany("EXO117231547");
		System.out.println("Company Delete: " + deleteCompany);
		
		/*Boolean createUser = createUser();
		System.out.println("User Created: " + createUser);*/
		
		/*Boolean updateUser = updateUser("AlampallyV_1548@fis.evincible.com", "Nageshwar", "Rao", "Akkineni");
		System.out.println("User Updated: " + updateUser);*/
		
		Boolean deleteUser = deleteUser("AlampallyV_1548@fis.evincible.com");
		System.out.println("User Delete: " + deleteUser);	

	}
	
	private static Boolean deleteUser(String ifsCode) {
		UserDeleteRequestType cdrt = new UserDeleteRequestType();
		
		UserDeleteRequestType.Body body = new UserDeleteRequestType.Body();
		cdrt.setBody(body);
		
		body.setIfsId(ifsCode);
		
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/user");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class, cdrt);
		System.out.println("clientResponse: " + clientResponse);
		ResponseType responseType = clientResponse.getEntity(ResponseType.class);
		System.out.println("responseType: " + responseType);
		if(responseType.getReturnCode().equals("0")) {
			return true;
		} else {
			return false;
		}
	}
	
	private static Boolean deleteCompany(String ifsCode) {
		CompanyDeleteRequestType cdrt = new CompanyDeleteRequestType();
		
		CompanyDeleteRequestType.Body body = new CompanyDeleteRequestType.Body();
		cdrt.setBody(body);
		
		body.setIfsId(ifsCode);
		
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/company");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class, cdrt);
		System.out.println("clientResponse: " + clientResponse);
		ResponseType responseType = clientResponse.getEntity(ResponseType.class);
		System.out.println("responseType: " + responseType);
		if(responseType.getReturnCode().equals("0")) {
			return true;
		} else {
			return false;
		}
	}
	
	private static Boolean updateCompany(String ifsCode, String iataCode) {
		CompanyCreateUpdateRequestType curt = new CompanyCreateUpdateRequestType();
		
		CompanyCreateUpdateRequestType.Body body = new CompanyCreateUpdateRequestType.Body();
		curt.setBody(body);
		body.setMode("UPDATE");
		CompanyType companyType = new CompanyType();		
		body.setCompany(companyType);
		companyType.setIfsId(ifsCode);
		companyType.setIataCode(iataCode);
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/company");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, curt);
		System.out.println("clientResponse: " + clientResponse);
		ResponseType responseType = clientResponse.getEntity(ResponseType.class);
		System.out.println("responseType: " + responseType);
		if(responseType.getReturnCode().equals("0")) {
			return true;
		} else {
			return false;
		}
	}
	
	private static Boolean updateUser(String ifsCode, String givenName, String middleName, String surName) {
		UserCreateUpdateRequestType curt = new UserCreateUpdateRequestType();
		
		UserCreateUpdateRequestType.Body body = new UserCreateUpdateRequestType.Body();
		curt.setBody(body);
		body.setMode("UPDATE");
		UserType userType = new UserType();		
		body.setUser(userType);
		userType.setIfsId(ifsCode);
		userType.setGivenName(givenName);
		userType.setSurname(surName);
		userType.setMiddleName(middleName);
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/user");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, curt);
		System.out.println("clientResponse: " + clientResponse);
		ResponseType responseType = clientResponse.getEntity(ResponseType.class);
		System.out.println("responseType: " + responseType);
		if(responseType.getReturnCode().equals("0")) {
			return true;
		} else {
			return false;
		}
	}

	private static Boolean createUser() {
		UserCreateUpdateRequestType ucurt = new UserCreateUpdateRequestType();
		
		UserCreateUpdateRequestType.Body body = new UserCreateUpdateRequestType.Body();
		ucurt.setBody(body);		
		UserType userType = new UserType();		
		body.setUser(userType);
		body.setMode("CREATE");
		
		userType.setBemsId("bemsId" + new SimpleDateFormat("mmss").format(new Date()));
		
		userType.setDisplayName("vivek4348");
		userType.setGivenName("Vivekanand");
		userType.setMiddleName("setMiddleName");
		userType.setSurname("Alampally");
		
		userType.setIfsId(userType.getSurname() + userType.getGivenName().charAt(0) + "_" + new SimpleDateFormat("mmss").format(new Date()) + "@fis.evincible.com".toLowerCase());
		
		userType.setEmailAddress("vivekanand.alampally@exostar.com");
		userType.setIfsAdministratorIndicator("true");
		userType.setCompanyIFSId("EXO2" + new SimpleDateFormat("ddHHmmss").format(new Date()));
		userType.setEmployeeRole("other");
		userType.setUserStatus("true");
		ServiceProviderType serviceProviderType1 = new ServiceProviderType();
		serviceProviderType1.setServiceProviderName("Active1");
		
		ServiceProviderType serviceProviderType2 = new ServiceProviderType();
		serviceProviderType2.setServiceProviderName("Active2");
		
			
		ServiceProviderType[] activeApplications = {serviceProviderType1, serviceProviderType2};
		userType.setActiveApplications(activeApplications);
		
		GregorianCalendar calendar = new GregorianCalendar();	
		XMLGregorianCalendar date = null;
		try {
			date = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		userType.setLastLogInDate(date);
		
		userType.setCreatedDate(date);
		userType.setGroupEmail("dev@exostar.com");
		userType.setCountryOfResidence("USA");
		userType.setCountryOfCitizenshipCode("IND");
		userType.setPhone("6184025145");
		userType.setMobilePhone("6184025145");
		userType.setJobRole("other");
		userType.setBusinessRole("other");
		userType.setDepartment("other");
		userType.setSuspendedDate(date);
		userType.setMainAccountUPN("");
		userType.setMainCompanyId("");
		
		System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/user");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, ucurt);
		System.out.println("clientResponse: " + clientResponse);
		ResponseType responseType = clientResponse.getEntity(ResponseType.class);
		System.out.println("responseType: " + responseType);
		if(responseType.getReturnCode().equals("0")) {
			return true;
		} else {
			return false;
		}				
	}

	
	private static Boolean createCompany() {
		CompanyCreateUpdateRequestType curt = new CompanyCreateUpdateRequestType();
		
		CompanyCreateUpdateRequestType.Body body = new CompanyCreateUpdateRequestType.Body();
		curt.setBody(body);		
		CompanyType companyType = new CompanyType();		
		body.setCompany(companyType);
		body.setMode("CREATE");
		
		companyType.setCompanyName("HardCoded Inc." + new SimpleDateFormat("ddHHmmss").format(new Date()));
		companyType.setIFSId("EXO1" + new SimpleDateFormat("ddHHmmss").format(new Date()));
		
		companyType.setCountry("USA");
		companyType.setCompanyStatus("true");
		ServiceProviderType serviceProviderType1 = new ServiceProviderType();
		serviceProviderType1.setServiceProviderName("Active1");
		
		ServiceProviderType serviceProviderType2 = new ServiceProviderType();
		serviceProviderType2.setServiceProviderName("Active2");
		
		ServiceProviderType serviceProviderType3 = new ServiceProviderType();
		serviceProviderType3.setServiceProviderName("Active3");
		
		ServiceProviderType serviceProviderType4 = new ServiceProviderType();
		serviceProviderType4.setServiceProviderName("Active4");
		
		ServiceProviderType[] activeApplications = {serviceProviderType1, serviceProviderType2};
		ServiceProviderType[] suspendedApplications ={serviceProviderType3, serviceProviderType4};
		
		/*activeApplications[0] = serviceProviderType1;
		activeApplications[1] = serviceProviderType2;
		suspendedApplications[2] = serviceProviderType3;
		suspendedApplications[3] = serviceProviderType4;*/
		
		
		companyType.setCompanyType("MRO");
		companyType.setICAOCode("ICAO");
		companyType.setIATACode("IATA");
		companyType.setCountryOfIncorporation("INDIA");
		companyType.setAgency("Agency");
		companyType.setSurrogateEmail("vivekanand.alapally@exostar.com");
		companyType.setActiveApplications(activeApplications);
		companyType.setSuspendedApplications(suspendedApplications);
		
		
		System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/company");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, curt);
		System.out.println("clientResponse: " + clientResponse);
		ResponseType responseType = clientResponse.getEntity(ResponseType.class);
		System.out.println("responseType: " + responseType);
		if(responseType.getReturnCode().equals("0")) {
			return true;
		} else {
			return false;
		}				
	}

	public static void mains(String[] args) {
		LinkedList<String> linkedList = new LinkedList<>();
		linkedList.add("Vivek");
		linkedList.add("Manasvi");
		linkedList.add("Sweety");
		linkedList.add("Vivek");
		linkedList.add("Sweety");
		Set<String> set = getDuplicates(linkedList);
		System.out.println(set);
	}

	private static String getAllAuthorizationsForUserAndSP(String userId, String spId) throws JAXBException {
		GetUserAuthorizationsRequestType getUserAuthorizationsRequestType = new GetUserAuthorizationsRequestType();
		
		GetUserAuthorizationsRequestType.Body body = new GetUserAuthorizationsRequestType.Body();
		body.setEndpointId("REQUEST.body.setEndpointId");		
		body.setIfsUserId(userId);
		body.setServiceProviderId(spId);
		//body.setServiceCollectionId("REQUEST.body.setServiceCollectionId");
		//body.setServiceId();
		getUserAuthorizationsRequestType.setBody(body);
		
		System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
		WebResource rsrc = cli.resource("http://localhost:8090/camservice/rsapi/getAllAuthorizationsOfServiceProviderForUser");
		ClientResponse clientResponse = rsrc.type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, getUserAuthorizationsRequestType);
		System.out.println(clientResponse);
		String jsonResponse = clientResponse.getEntity(String.class);
		System.out.println(jsonResponse);
		GetUserAuthorizationsResponseType getUserAuthorizationsResponseType = JsonUtil.getJsonMoxy(jsonResponse, GetUserAuthorizationsResponseType.class);
		String authorizationsJson = getUserAuthorizationsResponseType.getBody().getAuthorizationJSONString();
		return authorizationsJson;
	}
	
	
	
	public static void main1(String[] args) throws JAXBException {
		System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");		
		String jsonResponse = "{\"id\":1,\"name\":\"Ashraf\",\"skills\":[\"java\",\"sql\"]}";
		Employee getUserAuthorizationsResponseType = JsonUtil.getJsonMoxy(jsonResponse, Employee.class);
		System.out.println(getUserAuthorizationsResponseType);

	}
	
	public static <T> void printCollection(Collection<T> collection) {
		for (T t : collection) {
			System.out.println(t);
		}		 
	}
	
	public static <K,V> void printMap(Map<K,V> map) {
		for (Entry<K, V> entry: map.entrySet()) {
			System.out.println("Key: " + entry.getKey() + " Value: " + entry.getValue());
		}		 
	}
	
	public static <T> Set<T> getDuplicates(Collection<T> list) {
		Set<T> uniques = new HashSet<T>();
		return list.stream().filter(e -> !uniques.add(e)).collect(Collectors.toSet());
		}

	
	

}
