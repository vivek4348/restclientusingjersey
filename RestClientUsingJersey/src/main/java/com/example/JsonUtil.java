package com.example;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;

public class JsonUtil {
	
	public static <T> T getJsonMoxy(String jsonResponse, Class<T> respClass) throws JAXBException {
		JAXBContext jc = JAXBContext.newInstance(respClass);
		Unmarshaller unmarshaller = jc.createUnmarshaller();
		unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
		unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false);
		StreamSource json = new StreamSource(new StringReader(jsonResponse));
		T userResponse = unmarshaller.unmarshal(json, respClass).getValue();
		return userResponse;
	}

}
